Currently host names are hardcoded will change it soon. 

We need 2 GPU nodes for testing one for ps server and other for worker

ssh into node1 and run ps server

```
node1$ singularity exec ubuntu.img python3 distributed_TFexample.py --job_name="ps" --task_index=0

ssh into node2 and run worker

node2$ singularity exec ubuntu.img python3 distributed_TFexample.py --job_name="worker" --task_index=0
```
