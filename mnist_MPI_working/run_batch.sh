#!/bin/bash

#SBATCH --time=01:00:00   # walltime
#SBATCH --nodes=2   # number of nodes
##SBATCH --gres=gpu:1
#SBATCH --ntasks=2   # number of processor cores (i.e. tasks)
#SBATCH --partition=interactive
#SBATCH --mem-per-cpu=2560M   # memory per CPU core
#SBATCH -A p_scads


module load singularity
module load openmpi/3.0.0-gnu7.1

export SCOREP_ENABLE_TRACING=true
export SCOREP_ENABLE_PROFILING=true
export SCOREP_TOTAL_MEMORY=3G
export SCOREP_PROFILING_MAX_CALLPATH_DEPTH=200

srun singularity exec /scratch/p_scads/gocht/container/ubuntu_io.img python3 -m scorep --mpi mnist_replica.py
