Works for 1 Task/Node with clean exit. Extensions is easy by changing `tensorflow_on_slurm`

# Example:
```
srun -n2 -N2 -c6 -p interactive singularity exec ubuntu.img python3 mnist_replica.py
```
## Alternative
```
sbatch run_batch.sh
```